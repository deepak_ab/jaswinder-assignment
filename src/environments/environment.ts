// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDLNb58u-PRHv7P9GDB5q5sgVsvJ73W0I4",
    authDomain: "agendaboa-96c7d.firebaseapp.com",
    databaseURL: "https://agendaboa-96c7d.firebaseio.com",
    projectId: "agendaboa-96c7d",
    storageBucket: "",
    messagingSenderId: "1065174772199",
    appId: "1:1065174772199:web:3d16908666b5b9cf"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
