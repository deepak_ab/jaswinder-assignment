import { Component } from '@angular/core';
import { FirebaseAuth } from '@angular/fire';
import { Router } from '@angular/router';
import { AuthService } from './core/auth.service';
import { UserService } from './core/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  currentUser: any = {};
  constructor(public router: Router, private authService: AuthService, private userService: UserService) {
    this.userService.getCurrentUser().then(user => {
      this.router.navigate(['/home']);
    }, err => {
      this.router.navigate(['/login']);
    })
  }

  logout() {
    this.authService.doLogout().then(data => {
      this.router.navigate(['login']);
    })
  }
}
