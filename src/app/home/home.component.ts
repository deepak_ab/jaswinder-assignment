import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../services/firebase.service';
import { Router, Params } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  users: Array<any>;

  constructor(public firebaseService: FirebaseService, private router: Router) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.firebaseService.getUsers().then(result => {
      this.users = result;
    })
  }

  capitalizeFirstLetter(value) {
    return value.charAt(0).toUpperCase() + value.slice(1);
  }
}
