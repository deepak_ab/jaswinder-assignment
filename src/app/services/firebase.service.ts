import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import linq from "linq2fire";

import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  constructor(public db: AngularFirestore) { }

  getUsers() {
    const db = firebase.firestore();
    return linq(db).from('costs').where({
      'uid': localStorage.getItem('uid')
    }).orderBy({millis: 'desc'}).data();
  }

  createUser(value) {
    value.costValue = parseFloat(value.costValue);
    const currentDate = new Date(value.costDate).getDate() + '/' + (new Date(value.costDate).getMonth() + 1) + '/' + new Date(value.costDate).getFullYear();
    const currentDateMMDDYYYY = (new Date(value.costDate).getMonth() + 1) + '/' + new Date(value.costDate).getDate() + '/' + new Date(value.costDate).getFullYear();
    
    return this.db.collection('costs').add({
      date: currentDate,
      millis: new Date(currentDateMMDDYYYY).getTime(),
      millisUTC: (new Date(currentDateMMDDYYYY).getTime() - (new Date().getTimezoneOffset() * 60 * 1000)),
      value: value.costValue ? '$' + value.costValue.toFixed(2) : null,
      price: value.costValue ? parseFloat(value.costValue.toFixed(2)) : null,
      currency: 'USD',
      category: value.costCategory,
      description: value.descriptionOfCost,
      client: value.client,
      job: value.job,
      notes: value.notes,
      createdAt: new Date(),
      uid: localStorage.getItem('uid')
    });
  }
}
