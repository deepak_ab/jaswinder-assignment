import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { FirebaseService } from '../services/firebase.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss']
})
export class NewUserComponent implements OnInit {

  userForm: FormGroup;

  validation_messages = {
    'costDate': [
      { type: 'required', message: 'Cost Date is required.' }
    ],
    'costValue': [
      { type: 'required', message: 'Cost Value is required.' }
    ],
    'costCategory': [
      { type: 'required', message: 'Cost Category is required.' },
    ],
    'descriptionOfCost': [
      { type: 'required', message: 'Description Of Cost is required.' },
    ],
    'client': [
      { type: 'required', message: 'Client is required.' },
    ],
    'job': [
      { type: 'required', message: 'Job is required.' },
    ],
    'notes': [
      { type: 'required', message: 'Notes is required.' },
    ]
  };

  categoryList = ['Assistants', 'Food', 'Materials', 'Others', 'Taxes', 'Tools', 'Transport'];

  constructor(private fb: FormBuilder, public dialog: MatDialog, private router: Router, public firebaseService: FirebaseService) { }

  ngOnInit() {
    this.userForm = this.fb.group({
      costDate: [new Date(), Validators.required],
      costValue: ['', Validators.required],
      costCategory: ['Others', Validators.required],
      descriptionOfCost: [''],
      client: [''],
      job: [''],
      notes: [''],
    });
  }

  resetFields() {
    this.userForm.reset();
  }

  onSubmit(value) {
    this.firebaseService.createUser(value)
      .then(
        res => {
          this.resetFields();
          this.router.navigate(['/home']);
        }
      )
  }

}
